#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>

static const int BLOCK_SIZE = 256;
static const int N = 2000;

#define CUDA_CHECK_RETURN(value) {           \
    cudaError_t _m_cudaStat = value;         \
    if (_m_cudaStat != cudaSuccess) {        \
         fprintf(stderr, "Error %s at line %d in file %s\n",              \
                 cudaGetErrorString(_m_cudaStat), __LINE__, __FILE__);    \
         exit(1);                                                         \
       } }

__global__ void vadd (int *a, int *b, int *c, int N) {
    int id = blockIdx.x * blockDim.x + threadIdx.x;
    if (id < N) {
        c[id] = a[id] + b[id];
    }
}

int main () {
    // host (h*) and device (d*) pointers
    int *ha, *hb, *hc, *da, *db, *dc;
    int i;

    // allocate host memory
    ha = new int[N];
    hb = new int[N];
    hc = new int[N];

    // allocate device memory
    CUDA_CHECK_RETURN(cudaMalloc((void **) &da, N * sizeof(int)));
    CUDA_CHECK_RETURN(cudaMalloc((void **) &db, N * sizeof(int)));
    CUDA_CHECK_RETURN(cudaMalloc((void **) &dc, N * sizeof(int)));

    // initialize input vectors
    for (i = 0; i < N; i++) {
        ha[i] = rand () % 10000;
        hb[i] = rand () % 10000;
    }

    // copy input vectors to device
    CUDA_CHECK_RETURN(cudaMemcpy(da, ha, sizeof(int) * N, cudaMemcpyHostToDevice));
    CUDA_CHECK_RETURN(cudaMemcpy(db, hb, sizeof(int) * N, cudaMemcpyHostToDevice));

    // run kernel
    int grid = ceil(N * 1.0 / BLOCK_SIZE);
    vadd <<< grid, BLOCK_SIZE >>> (da, db, dc, N);
    CUDA_CHECK_RETURN(cudaDeviceSynchronize());
    CUDA_CHECK_RETURN(cudaGetLastError());

    // copy output vector to host
    CUDA_CHECK_RETURN(cudaMemcpy(hc, dc, sizeof (int) * N, cudaMemcpyDeviceToHost));

    // check results
    for (i = 0; i < N; i++) {
        if (hc[i] != ha[i] + hb[i]) {
            printf("Error at index %i : %i != %i\n", i, hc[i], ha[i] + hb[i]);
        }
    }

    // free GPU memory
    CUDA_CHECK_RETURN(cudaFree(da));
    CUDA_CHECK_RETURN(cudaFree(db));
    CUDA_CHECK_RETURN(cudaFree(dc));
    
    // free CPU memory
    delete[] ha;
    delete[] hb;
    delete[] hc;

    // clean up device resources
    CUDA_CHECK_RETURN(cudaDeviceReset());

    return 0;
}