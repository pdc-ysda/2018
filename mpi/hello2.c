#include <mpi.h>
#include <stdio.h>

int main(int argc, char* argv[]) {
    int rank, size, len, tag = 1;
    char host[MPI_MAX_PROCESSOR_NAME];
    char msg[50];
    
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Get_processor_name(host, &len);

    if (rank == 0) {
        int i;
        MPI_Status status;
        printf("Hello, world. I am %d of %d on %s\n", rank, size, host);
        for (i = 1; i < size; i++) {
            MPI_Recv(msg, 50, MPI_CHAR, MPI_ANY_SOURCE, tag, MPI_COMM_WORLD, &status);
            printf("Msg from %d: '%s'\n", status.MPI_SOURCE, msg);
        }
    } else {
        snprintf(msg, 50, "Hello, master. I am %d of %d on %s", rank, size, host);
        MPI_Send(msg, 50, MPI_CHAR, 0, tag, MPI_COMM_WORLD);
    }

    MPI_Finalize();
    return 0;
}