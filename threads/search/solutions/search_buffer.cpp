#include <algorithm>
#include <chrono>
#include <fstream>
#include <future>
#include <iostream>
#include <queue>
#include <string>
#include <thread>
#include <vector>

#define TIMING

#ifdef TIMING
#define INIT_TIMER auto start = std::chrono::high_resolution_clock::now();
#define RESET_TIMER start = std::chrono::high_resolution_clock::now();
#define STOP_TIMER(name)                                                                           \
    std::cout << name << " RUNTIME: "                                                              \
              << std::chrono::duration_cast<std::chrono::milliseconds>(                            \
                     std::chrono::high_resolution_clock::now() - start)                            \
                     .count()                                                                      \
              << " ms " << std::endl;
#else
#define INIT_TIMER
#define RESET_TIMER
#define STOP_TIMER(name)
#endif

class BoundedBuffer {
public:
    BoundedBuffer(int capacity)
        : capacity(capacity)
        , head(0)
        , tail(0)
        , count(0) {
        buffer = new std::string[capacity];
    }

    ~BoundedBuffer() {
        delete[] buffer;
    }

    void put(std::string data) {
        std::unique_lock<std::mutex> lock(mutex);

        not_full.wait(lock, [this]() { return count != capacity; });

        buffer[tail] = data;
        tail = (tail + 1) % capacity;
        ++count;

        not_empty.notify_one();
    }

    std::string take() {
        std::unique_lock<std::mutex> lock(mutex);

        not_empty.wait(lock, [this]() { return count != 0; });

        std::string result = buffer[head];
        head = (head + 1) % capacity;
        --count;

        not_full.notify_one();

        return result;
    }

private:
    std::string* buffer;
    int capacity;

    int head;
    int tail;
    int count;

    std::mutex mutex;
    std::condition_variable not_full;
    std::condition_variable not_empty;
};

std::vector<std::string> search(std::string needle, BoundedBuffer& queue) {
    std::vector<std::string> results;
    std::string str;
    while ((str = queue.take()) != "POISON") {
        std::transform(str.begin(), str.end(), str.begin(), ::tolower);
        if (str.find(needle) != std::string::npos) {
            results.push_back(str);
        }
    }
    return results;
}

int main(int argc, char* argv[]) {
    if (argc != 3) {
        std::fprintf(stderr, "Usage: %s file needle\n", argv[0]);
        return 1;
    }
    std::ifstream fs(argv[1]);
    std::string needle = argv[2];

    INIT_TIMER

    BoundedBuffer queue(10000);
    int num_proc = std::thread::hardware_concurrency();
    std::cout << "Using " << num_proc << " threads" << std::endl;
    std::vector<std::future<std::vector<std::string>>> futures;
    for (int i = 0; i < num_proc; i++) {
        futures.push_back(std::async(std::launch::async, search, needle, std::ref(queue)));
    }

    for (std::string line; std::getline(fs, line);) {
        queue.put(line);
    }

    for (int i = 0; i < num_proc; i++) {
        queue.put("POISON");
    }

    int count = 0;
    for (int i = 0; i < num_proc; i++) {
        std::vector<std::string> res = futures.at(i).get();
        // for (std::string str : res) {
        //   std::cout << str << std::endl;
        //}
        count += res.size();
    }
    std::cout << "Found " << count << " matches" << std::endl;

    STOP_TIMER("READ/SEARCH")
}